package browserFactory;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class DriversFactory {

    protected static WebDriver wd;
    RemoteWebDriver driver;
	public  ExtentReports report;
	public ExtentTest logger;
	public String dateTime = new SimpleDateFormat("yyyyMMdd-hha").format(new Date());
	
   
   /* @Rule
	public BrowserWebDriverContainer chrome =
			new BrowserWebDriverContainer()
            .withDesiredCapabilities(DesiredCapabilities.chrome());*/
	
	
    /*public DriversFactory() {
        initialize();
    }*/

   /* public void initialize() {
    	
        String url = null;
		String browser = null;
		if (wd == null)
        	chooseDriver(url, browser);
    }
*/
    
    
    public  WebDriver chooseDriver(
   			 
    			String url, 
    			String browser) {
    		try {
    			if (browser.equalsIgnoreCase("Firefox")) {
    				DesiredCapabilities caps = new DesiredCapabilities();

    				 caps.setJavascriptEnabled(true);

    				
    				wd = new FirefoxDriver();
    			} else if (browser.equalsIgnoreCase("Chrome")) {
    				DesiredCapabilities caps = new DesiredCapabilities();

    				 caps.setJavascriptEnabled(true);

    			 caps.setCapability("takesScreenshot", true);
    				
    				System.setProperty("webdriver.chrome.driver", "./driver/chromedriver");
    				wd = new ChromeDriver();
    				wd.manage().deleteAllCookies();
    				//driver=chrome.getWebDriver();
    			} else if (browser.equalsIgnoreCase("IE")) {
    				System.setProperty("webdriver.ie.driver", "./driver/IEDriverServer.exe");
    				wd = new InternetExplorerDriver();}
    			 else if (browser.equalsIgnoreCase("Headless")) {
    				 /*Capabilities caps = new DesiredCapabilities();
    				 ((DesiredCapabilities) caps).setJavascriptEnabled(true);
    				 ((DesiredCapabilities) caps).setCapability("takesScreenshot", false);
    				 //((DesiredCapabilities) caps).setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, newString[] {"--web-security=no", "--ignore-ssl-errors=yes"});
    				 ((DesiredCapabilities) caps).setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"./driver/phantomjs");

    						
    				 //System.setProperty("phantomjs.binary.path", "./driver/phantomjs");
     			     wd = new PhantomJSDriver();
    			*/
    				 DesiredCapabilities caps = new DesiredCapabilities();
    				 caps.setJavascriptEnabled(true);                
    				 caps.setCapability("takesScreenshot", true);  
    				 caps.setCapability(
    				                         PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
    				                         "./driver/phantomjs"
    				                     );
    				 wd = new  PhantomJSDriver(caps);
    				
    				
    				    
    			}else if (browser.equalsIgnoreCase("Edge")) {
    				System.setProperty("webdriver.Microsoft.driver", "./driver/MicrosoftWebDriver.exe");
    				wd = new EdgeDriver();
    				
    			}
    			wd.get(url);
    			wd.manage().window().maximize();
    			wd.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    			return wd;
    		} catch (WebDriverException e) {
    			//logger.log(LogStatus.INFO,e.getMessage());
    			System.out.println(e.getMessage());
    			return wd;}
    		}
    		
    		
    public void getScreenFailed(){
		String screenshotFile = ".//screenshot//IntegrationTest_" + dateTime + "\\"+  ".png";
		
			try {
				TakesScreenshot ts = (TakesScreenshot)wd;
				File source = ts.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(source, new File(screenshotFile));
				
				
				logger.log(LogStatus.INFO, "Something went wrong with the test! Screenshot taken!!\n");
				logger.log(LogStatus.INFO, "Screenshot below: " + logger.addScreenCapture("." + screenshotFile));
			} catch (Exception e) {
				logger.log(LogStatus.INFO,"Exception while taking screenshot " + e.getMessage() + "\n");
			}
		}
		
		
    

    public WebDriver getDriver() {
    	return wd;	
    	
    }
    
    public void destroyDriver() {
    	if (wd == null) {
	        return;
	    }
	    wd.quit();
	    wd = null;
        
    }
    
    
	
    
}