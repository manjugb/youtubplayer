package pageObjects;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.jsoup.select.Evaluator.IsEmpty;
import org.junit.gen5.api.Assertions;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserFactory.DriversFactory;



public class youTube extends DriversFactory {
	
	public browserFactory.DriversFactory df = new browserFactory.DriversFactory();
	
	
	public WebDriver driver;
	public static final String ANSI_RED_TEXT = "\033[31m";
	public static final String ANSI_GREEN_TEXT = "\033[32;1;2m";
	public youTube(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}
	
	//Search Bar
	 @FindBy(how=How.XPATH,using="//input[@id='search']")
	 WebElement elmSearchBar;
	 
	 //SearchButton
	 @FindBy(how=How.XPATH,using="//button[@id='search-icon-legacy']")
	 WebElement elmClickSearch;
	 
	 //Results
	 @FindBy(how=How.XPATH,using=" //yt-showing-results-for-renderer[@class='style-scope ytd-item-section-renderer']")
	 WebElement elmResults;
	 
	 @FindBy(how=How.XPATH,using="//button[@id='search-icon-legacy']//yt-icon[@class='style-scope ytd-searchbox']")
	 WebElement elmClickSearch_chrm;
	 
	 public void verifyKeyword(String strExpKeyword) {
		 String isTheTextPresent = driver.getPageSource();
		 if(isTheTextPresent.contains(strExpKeyword)) {
			 Assertions.assertTrue(isTheTextPresent.contains(strExpKeyword)); 
			 System.out.println(ANSI_GREEN_TEXT+""+strExpKeyword+"" + "Found");
			 //logger.log(LogStatus.PASS,"Passed");
		 }else {
			 Assertions.assertFalse(isTheTextPresent.contains(strExpKeyword)); 
			 System.out.println(ANSI_RED_TEXT+""+strExpKeyword+"" +"Not Found");
			 df.getScreenFailed();
		    
		 }
		
		 
	 }
	 
	 		
	 //Enter Keyword
	 public void enter_key(String keyword) throws Throwable{
		
		 elmSearchBar.clear();
		 elmSearchBar.sendKeys(keyword);
		 
	 }
	 
	 public void press_enter() throws Throwable{
		 elmSearchBar.sendKeys(Keys.ENTER);
	 }
	 
	 //Click on Search Button
	 public void clk_searchbut() throws Throwable{
		 
		 elmClickSearch.click();
		 
		
	 }
	 
	 
	 
public void clk_searchbut_chrome() throws Throwable{
		 
	//elmClickSearch_chrm.click();

	Actions action = new Actions(driver);
	action.moveToElement(elmClickSearch_chrm).perform();
	elmClickSearch_chrm.click();
		
	 }
	 
	 
		 
	
	 
	 
}
