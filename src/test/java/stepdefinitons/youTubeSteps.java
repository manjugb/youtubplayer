package stepdefinitons;

import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import browserFactory.DriversFactory;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import pageObjects.youTube;

public class youTubeSteps extends DriversFactory{
	protected ExtentTest logger;
	public browserFactory.DriversFactory df = new browserFactory.DriversFactory();
	
	@Given("^I Go to \"([^\"]*)\" on \"([^\"]*)\"$") 
	public void open_browser(String url,String browser) throws Throwable{
		if(wd==null && browser.equalsIgnoreCase("Firefox")) {
		df.chooseDriver(url, browser);
		df.getDriver().getTitle();
		System.out.println(wd.hashCode());
		}else if(wd==null && browser.equalsIgnoreCase("Chrome")) {
			df.chooseDriver(url, browser);
			df.getDriver().getTitle();
			System.out.println(wd.hashCode());
		}else if(wd==null && browser.equalsIgnoreCase("Headless")) {
			df.chooseDriver(url, browser);
			df.getDriver().getTitle();
			System.out.println(wd.hashCode());
		}
		
					
	 }
	
	@Then("^I Enter Keyword to Search \"([^\"]*)\"$")
	public void enter_key(String key) throws Throwable{
	
		new youTube(wd).enter_key(key);
		
	}
	
	@Then("I Press Enter$")
	public void pres_enter() throws Throwable{
		new youTube(wd).press_enter();
	}
	
	@Then("^I Click Search Button$")
	public void clk_Search() throws Throwable{
		
		new youTube(wd).clk_searchbut();
	}
	
	@Then("^I Click Search Button Chrome$")
	public void clk_Search_Chrome() throws Throwable{
		new youTube(wd).clk_searchbut_chrome();
	}
	
    
	@Then("^I Verify Keyword \"([^\"]*)\"$")
	public void verifytext(String expText) throws Throwable{
		new youTube(wd).verifyKeyword(expText);
	}
	
	@Then("^I Close Browser$")
	public void close_br() throws Throwable{
		df.destroyDriver();
	
	}
	@Then("^I Wait for some Time$")
	public void waitfor() {
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
