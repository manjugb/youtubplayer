package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;



public class youTube extends BasePage {
	
	public pages.DriverHandler dh = new pages.DriverHandler();
	
	
	public WebDriver driver;
	public static final String ANSI_RED_TEXT = "\033[31m";
	public static final String ANSI_GREEN_TEXT = "\033[32;1;2m";
	public youTube(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);

	}
	
	//Search Bar
	 @FindBy(how=How.XPATH,using="//input[@id='search']")
	 WebElement elmSearchBar;
	 
	//Search Bar
		String elmSearch;
	 //SearchButton
	 @FindBy(how=How.XPATH,using="//button[@id='search-icon-legacy']")
	 WebElement elmClickSearch;
	 
	 //Results
	 @FindBy(how=How.XPATH,using=" //yt-showing-results-for-renderer[@class='style-scope ytd-item-section-renderer']")
	 WebElement elmResults;
	 
	 @FindBy(how=How.XPATH,using="//button[@id='search-icon-legacy']//yt-icon[@class='style-scope ytd-searchbox']")
	 WebElement elmClickSearch_chrm;
	 
	
	 		
	 //Enter Keyword
	 public void enter_key(String keyword) throws Throwable{
		
		 elmSearchBar.clear();
		 elmSearchBar.sendKeys(keyword);
		 
	 }
	 
	 public void press_enter() throws Throwable{
		 elmSearchBar.sendKeys(Keys.ENTER);
	 }
	 
	 //Click on Search Button
	 public void clk_searchbut() throws Throwable{
		 
		 elmClickSearch.click();
		 
		
	 }
	 
	 
	 
public void clk_searchbut_chrome() throws Throwable{
		 
	//elmClickSearch_chrm.click();

	Actions action = new Actions(driver);
	action.moveToElement(elmClickSearch_chrm).perform();
	elmClickSearch_chrm.click();
		
	 }

private youTube ensurePageLoaded() {
    waitForPageToLoad();
    try {
        getElementByCSS(elmSearch);
    } catch (TimeoutException e) {
        System.out.println("Timeout exceeded while waiting for " + this.getClass().getSimpleName() + " to load!");
    }
    return this;
}


@Override 
public boolean pageHasText(String text) {
   return isElementPresent(By.xpath("//*[contains(text(), '" + text + "')]"));
}

//boolean isElementPresent(By by) {
//	try{
//		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
//	} catch(TimeoutException e) {
//		return false;
//	}
//	return true;
//}





public void verifyKeyword(String strExpKeyword) throws Throwable{
	if(pageHasText(strExpKeyword)) {
		Assert.assertTrue(pageHasText(strExpKeyword), "Keyword Found");
		 System.out.println(ANSI_GREEN_TEXT+""+strExpKeyword+"" + "Found");
	}else {
		  Assert.assertFalse(pageHasText(strExpKeyword), "Keyword not Found");
		 System.out.println(ANSI_GREEN_TEXT+""+strExpKeyword+"" + "Not Found");
		
	}
}



@Override
public boolean isPageLoaded() {
    return ensurePageLoaded().isElementPresent(By.cssSelector(elmSearch));
}
}
		 
	
	 
	 

