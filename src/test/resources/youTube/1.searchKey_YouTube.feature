Feature:  As a Youtube user i will go and search my favorite singer
Scenario Outline: Validate search key on Firefox

Given I Go to "<url>" on "<Browser>"
Then I Wait for some Time
Then I Enter Keyword to Search "<key>"
Then I Wait for some Time
Then I Click Search Button
Then I Wait for some Time
And I Verify Keyword "<expkey>"

Examples:
|url|Browser|key|expkey|
|https://www.youtube.com|Firefox|ms subbalakshmi|ms subbalakshmi|
|https://www.youtube.com|Firefox|Saluri Koteswara Rao|Saluri Koteswara Rao|
|https://www.youtube.com|Firefox|chiranjeevi|chiranjeevi|
|https://www.youtube.com|Firefox|sunitha|sunitha|
|https://www.youtube.com|Firefox|bombay jayasri|bombay jayasri|
|https://www.youtube.com|Firefox|suchitra|suchitra|

Scenario: Validate search key  Close Firefox
Then I Close Browser


Scenario Outline: Validate search key on Chrome

Given I Go to "<url>" on "<Browser>"
Then I Enter Keyword to Search "<key>"
Then I Wait for some Time
Then I Press Enter
Then I Wait for some Time
And I Verify Keyword "<expkey>"
Then I Wait for some Time

Examples:
|url|Browser|key|expkey|
|https://www.youtube.com|Chrome|ms subbalakshmi|ms subbalakshmi|
|https://www.youtube.com|Chrome|Saluri Koteswara Rao|Saluri Koteswara Rao|
|https://www.youtube.com|Chrome|chiranjeevi|chiranjeevi|
|https://www.youtube.com|Chrome|sunitha|sunitha|
|https://www.youtube.com|Chrome|bombay jayasri|bombay jayasri|
|https://www.youtube.com|Chrome|suchitra|suchitra|

Scenario: Validate search key  Close Chrome
Then I Close Browser